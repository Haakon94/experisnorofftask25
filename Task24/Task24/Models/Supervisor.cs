﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models {
    public class Supervisor {

        [Required(ErrorMessage = "Please enter an number")]
        [RegularExpression("^\\d+$", ErrorMessage = "Please enter a valid whole number")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please indicate availability")]
        public bool? IsAvailable { get; set; }

    }
}
