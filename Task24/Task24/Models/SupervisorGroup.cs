﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models {
    public static class SupervisorGroup {
        private static List<Supervisor> CurrentSupervisors = new List<Supervisor>();

        public static List<Supervisor> Supervisors {
            get { return CurrentSupervisors; }
        }

        public static void AddSupervisor(Supervisor newSupervisor) {
            CurrentSupervisors.Add(newSupervisor);
        }
    }
}
